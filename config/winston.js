let appRoot = require('app-root-path');
let winston = require('winston');

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;


const myFormat = printf(info => {
  return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
});

var options = {
    file: {
      level: 'info',
      filename: `./logs/app.log`,
      handleExceptions: true,
      json: true,
      maxsize: 5242880, // 5MB
      maxFiles: 5,
      timestamp:timestamp()
    },
    errFile: {
      level: 'error',
      filename: `./logs/error.log`,
      handleExceptions: true,
      json: true,
      maxsize: 5242880, // 5MB
      maxFiles: 5,
      colorize: true,
      timestamp:timestamp()
    },
    console: {
      level: 'info',
      handleExceptions: true,
      json: true,
      colorize: true,
      timestamp:timestamp()
    },
  };


  winston.addColors({
    info: 'blue',
    error:'red',
    warn:'yellow'
});

  var logger = createLogger({
    transports: [
      new transports.File(options.file),
      new transports.File(options.errFile),
      new transports.Console({
        format: combine(
          winston.format.colorize(),
          label({ label: 'client' }),
          timestamp(),
          myFormat
        ),
        transports: [new transports.Console()]
      })
    ],
    exitOnError: false, // do not exit on handled exceptions
  });



  logger.stream = {
    write: function(message, encoding) {
      logger.info(message);
    },
  };


  module.exports = logger;

