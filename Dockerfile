FROM node:8-alpine
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . .
RUN NPM INSTALL
EXPOSE 8081
CMD ["node", "server.js"]