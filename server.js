let express = require('express');
let morgan = require('morgan');
let logger = require('./config/logger');
let app = express();

app.use(morgan('combined', { stream: logger.stream }));


app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // add this line to include logger logging
    logger.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });


app.get('/',(req,res)=>{
    logger.log('info',"request received");

    logger.log('warn','client error occured')
try{
res1.send("Node Docker");
}catch(e){
    logger.log('error','client error occured here')
    res.send("Error");
}
});

app.listen(8081,()=>{
    console.log("app running in port 8081")
});