Feature: Is it friday yet?
 Everybody want to know when is friday?

 Scenario:Sunday isn't friday
   Given today is Sunday
   When I ask whether it's friday yet
   Then I should be told "Nope"

 Scenario: Friday is Friday
 Given today is Friday
 When I ask whether it's Friday yet
 Then I should get "Yes"
